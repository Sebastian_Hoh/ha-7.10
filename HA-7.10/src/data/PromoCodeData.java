package data;
import java.io.BufferedWriter; 
import java.io.FileWriter; 
import java.io.IOException; 
import java.io.PrintWriter; 

import java.util.LinkedList;
import java.util.List;

public class PromoCodeData implements IPromoCodeData {
	String username = System.getProperty("user.name");
	private List<String> promocodes;
	
	public PromoCodeData(){
		this.promocodes = new LinkedList<String>();
	}
	
	@Override
	public boolean savePromoCode(String code) {
		try {
			PrintWriter pWriter = new PrintWriter(new FileWriter("C:\\Users\\"+username+"\\Desktop\\PromoCodes.txt",true), true);
			pWriter.println(code); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this.promocodes.add(code);
	}

	@Override
	public boolean isPromoCode(String code) {
		return this.promocodes.contains(code);
	}

}

package gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.IPromoCodeLogic;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class PromocodeGUI extends JFrame {

	private static final long serialVersionUID = 2303911575322895620L;
	private JPanel contentPane;
	private JLabel lblPromocode;

	/**
	 * Create the frame.
	 */
	public PromocodeGUI(IPromoCodeLogic logic) {
		setForeground(new Color(0, 102, 102));
		setBackground(new Color(0, 0, 0));
		setTitle("PromotionCodeGenerator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 0, 51));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{474, 0};
		gbl_contentPane.rowHeights = new int[]{83, 0, 83, 0, 83, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblBeschriftungPromocode = new JLabel("Promotioncode f�r 2-fach IT$ in einer Stunde");
		lblBeschriftungPromocode.setForeground(new Color(0, 0, 0));
		lblBeschriftungPromocode.setBackground(new Color(0, 0, 0));
		lblBeschriftungPromocode.setAlignmentY(Component.TOP_ALIGNMENT);
		lblBeschriftungPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		lblBeschriftungPromocode.setFont(new Font("Tahoma", Font.BOLD, 18));
		GridBagConstraints gbc_lblBeschriftungPromocode = new GridBagConstraints();
		gbc_lblBeschriftungPromocode.gridheight = 2;
		gbc_lblBeschriftungPromocode.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblBeschriftungPromocode.insets = new Insets(0, 0, 5, 0);
		gbc_lblBeschriftungPromocode.gridx = 0;
		gbc_lblBeschriftungPromocode.gridy = 0;
		contentPane.add(lblBeschriftungPromocode, gbc_lblBeschriftungPromocode);
		
		lblPromocode = new JLabel("");
		lblPromocode.setForeground(new Color(255, 153, 255));
		lblPromocode.setBackground(new Color(0, 0, 0));
		lblPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromocode.setFont(new Font("Baskerville Old Face", Font.BOLD, 22));
		GridBagConstraints gbc_lblPromocode = new GridBagConstraints();
		gbc_lblPromocode.gridheight = 2;
		gbc_lblPromocode.fill = GridBagConstraints.BOTH;
		gbc_lblPromocode.insets = new Insets(0, 0, 5, 0);
		gbc_lblPromocode.gridx = 0;
		gbc_lblPromocode.gridy = 2;
		contentPane.add(lblPromocode, gbc_lblPromocode);
		
		JButton btnNewPromoCode = new JButton("Erstellen und Speichern eines neuen Promocodes");
		btnNewPromoCode.setBackground(new Color(51, 0, 51));
		btnNewPromoCode.setForeground(new Color(255, 255, 255));
		btnNewPromoCode.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewPromoCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String code = logic.getNewPromoCode();
				lblPromocode.setText(code);
			}
		});
		GridBagConstraints gbc_btnNewPromoCode = new GridBagConstraints();
		gbc_btnNewPromoCode.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewPromoCode.anchor = GridBagConstraints.SOUTH;
		gbc_btnNewPromoCode.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewPromoCode.gridx = 0;
		gbc_btnNewPromoCode.gridy = 4;
		contentPane.add(btnNewPromoCode, gbc_btnNewPromoCode);
		this.setVisible(true);
	}

}
